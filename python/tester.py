import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class PythonOrgSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()

    # Testet startar
    def test_search_in_python_org(self):
        driver = self.driver

        # Laddar in hemsidan
        driver.get("https://pontusgranath.gitlab.io/projekt-spa/")

        # Kollar efter texter
        self.assertIn("Stillhetens spa", driver.title)
        welcome = driver.find_element_by_tag_name("h1")
        assert welcome.text == "Välkommen till Stillhetens spa"

        oppettider = driver.find_element_by_class_name("oppettider")
        assert oppettider.find_element_by_tag_name("h2").text == "Öppettider"
        oppettider.find_element_by_class_name("oppet_dag")
        oppettider.find_element_by_css_selector("oppet_dag, p:nth-child(1)")
        oppettider.find_element_by_css_selector("oppet_dag, p:nth-child(2)")
        oppettider.find_element_by_css_selector("oppet_dag, p:nth-child(3)")
        oppettider.find_element_by_class_name("oppet_tid")
        oppettider.find_element_by_css_selector("oppet_tid, p:nth-child(1)")
        oppettider.find_element_by_css_selector("oppet_tid, p:nth-child(2)")
        oppettider.find_element_by_css_selector("oppet_tid, p:nth-child(3)")

        produkter = driver.find_element_by_class_name("produkter")
        produkter.find_element_by_class_name("produktprod")
        produkter.find_element_by_css_selector("produktprod:nth-child(1), p:nth-child(1)")
        produkter.find_element_by_css_selector("produktprod:nth-child(1), p:nth-child(2)")
        produkter.find_element_by_css_selector("produktprod:nth-child(1), p:nth-child(3)")
        produkter.find_element_by_css_selector("produktprod:nth-child(1), p:nth-child(4)")
        produkter.find_element_by_css_selector("produktprod:nth-child(2), p:nth-child(1)")
        produkter.find_element_by_css_selector("produktprod:nth-child(2), p:nth-child(2)")
        produkter.find_element_by_css_selector("produktprod:nth-child(3), p:nth-child(1)")
        produkter.find_element_by_css_selector("produktprod:nth-child(3), p:nth-child(2)")
        produkter.find_element_by_class_name("produktpris")
        produkter.find_element_by_css_selector("produktpris:nth-child(1), p:nth-child(1)")
        produkter.find_element_by_css_selector("produktpris:nth-child(1), p:nth-child(2)")
        produkter.find_element_by_css_selector("produktpris:nth-child(1), p:nth-child(3)")
        produkter.find_element_by_css_selector("produktpris:nth-child(1), p:nth-child(4)")
        produkter.find_element_by_css_selector("produktpris:nth-child(2), p:nth-child(1)")
        produkter.find_element_by_css_selector("produktpris:nth-child(2), p:nth-child(2)")
        produkter.find_element_by_css_selector("produktpris:nth-child(3), p:nth-child(1)")
        produkter.find_element_by_css_selector("produktpris:nth-child(3), p:nth-child(2)")

        kontakt = driver.find_element_by_class_name("kontakt")
        kontakt.find_element_by_css_selector("kontakt, p:nth-child(1)")
        kontakt.find_element_by_css_selector("kontakt, p:nth-child(2)")
        kontakt.find_element_by_css_selector("kontakt, p:nth-child(3)")
        kontakt.find_element_by_css_selector("kontakt, p:nth-child(4)")
        kontakt.find_element_by_tag_name("a")

        # Kollar efter bild classerna
        driver.find_elements_by_id("img-container")
        driver.find_element_by_class_name("img-set")
        driver.find_element_by_css_selector("img-set, img:first-child")
        driver.find_element_by_css_selector("img-set, img:last-child")
        driver.find_element_by_class_name("lone-img")

        # Kollar efter länkarna
        driver.find_element_by_css_selector("socials, a:nth-child(1)")
        driver.find_element_by_css_selector("socials, a:nth-child(2)")
        driver.find_element_by_css_selector("socials, a:nth-child(3)")



        assert "No results found." not in driver.page_source

if __name__ == "__main__":
    unittest.main()