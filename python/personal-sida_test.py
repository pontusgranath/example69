import unittest
from selenium import webdriver

class PythonOrgSearch(unittest.TestCase):

# ALLA NAMN I DETTA TEST ÄR PLACEHOLDERS

    def setUp(self):
        self.driver = webdriver.Firefox()

    # Testet startar
    def test_search_in_python_org(self):
        driver = self.driver

        # Laddar in hemsidan ÄNDRA SEN
        driver.get("https://PLACEHOLDER.se")

        welcome = driver.find_element_by_tag_name("h1")
        assert welcome.text == "Personal"

        # Tillbaks knappen för att KOMMA TILLBAKA till startsidan
        back = driver.find_element_by_class_name("back")
        back.find_element_by_tag_name("a")

        bilder = driver.find_element_by_class_name("bilder")
        namn = driver.find_element_by_class_name("namn")

        assert "No results found." not in driver.page_source

if __name__ == "__main__":
    unittest.main()